# PHPUNIT on docker
Docker phpunit setup from : https://www.shiphp.com/blog/2017/phpunit-docker 


## Use
shell scrips make this quicker

```
./setup.sh #init setup of the docker modules
./test.sh #run one off test
./watch.sh #watch modules dir and run tests on change
```


## raw commands

Install
```
docker run --rm -v $(pwd):/app composer/composer:latest require --dev phpunit/phpunit ^6.0

```

Run test
```
docker run -v $(pwd):/app --rm phpunit/phpunit:latest --bootstrap _ENTRY-OR-FILE_.php _TEST-FILE-OR-FOLDER_
```
